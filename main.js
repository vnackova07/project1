//BURGER ANIMATION
const menuBtn = document.querySelector("#burgerImage");
const navList = document.querySelector("ul");
let menuOpen = false;
menuBtn.addEventListener("click", () => {
  if (!menuOpen) {
    menuBtn.classList.add("open");
    navList.classList.add("open");
    document.getElementById("burgerImage").src="./images/burgermenuopen.svg";
    document.getElementById("burgerImage").style.width = "25px";
    document.getElementById("mainLogo").style.visibility = "hidden";
    document.querySelector("nav").style.backgroundColor = "#302f38";
    document.querySelector("body").style.overflow = "hidden";
    menuOpen = true;
  } else {
    menuBtn.classList.remove("open");
    navList.classList.remove("open");
    document.getElementById("burgerImage").src="./images/burgermenu.svg";
    document.getElementById("burgerImage").style.width = "35px";
    document.getElementById("mainLogo").style.visibility = "visible";
    document.querySelector("nav").style.backgroundColor = "#fcd232";
    document.querySelector("body").style.overflow = "inherit";
    menuOpen = false;
  }
});

var labelsArray = document.getElementsByName("categories");
var imageSelected = document.getElementsByTagName("i");

filterMarketing();

document.querySelector("#filter-coding").addEventListener("change", filterCoding);
        document.querySelector("#filter-design").addEventListener("change", filterDesign);
        document.querySelector("#filter-marketing").addEventListener("change", filterMarketing);
        

        function filterCoding() {
            hideAllCards();

            if(document.querySelector("#filter-coding").checked) {
                labelsArray[0].classList.remove('bg-danger', 'text-dark');
                labelsArray[1].classList.add('bg-danger', 'text-dark');
                labelsArray[2].classList.remove('bg-danger', 'text-dark');
                imageSelected[0].style.visibility = "hidden";
                imageSelected[1].style.visibility = "visible";
                imageSelected[2].style.visibility = "hidden";
                var codingCards = document.querySelectorAll(".coding");
                codingCards.forEach(codingCard => {
                    codingCard.style.display = "inline-block";
                });

                document.querySelector("#filter-design").checked = false;
                document.querySelector("#filter-marketing").checked = false;
            } else {
                showAllCards();
            }
        }

        function filterDesign() {
            hideAllCards();
            

            if(document.querySelector("#filter-design").checked) {
                labelsArray[0].classList.remove('bg-danger', 'text-dark');
                labelsArray[1].classList.remove('bg-danger', 'text-dark');
                labelsArray[2].classList.add('bg-danger', 'text-dark');
                imageSelected[0].style.visibility = "hidden";
                imageSelected[1].style.visibility = "hidden";
                imageSelected[2].style.visibility = "visible";
                var designCards = document.querySelectorAll(".design");
                designCards.forEach(designCard => {
                    designCard.style.display = "inline-block";
                });

                document.querySelector("#filter-coding").checked = false;
                document.querySelector("#filter-marketing").checked = false;
            } else {
                showAllCards();
            }
        }

        function filterMarketing() {
            hideAllCards();

            if(document.querySelector("#filter-marketing").checked) {
                labelsArray[0].classList.add('bg-danger', 'text-dark');
                labelsArray[1].classList.remove('bg-danger', 'text-dark');
                labelsArray[2].classList.remove('bg-danger', 'text-dark');
                imageSelected[0].style.visibility = "visible";
                imageSelected[1].style.visibility = "hidden";
                imageSelected[2].style.visibility = "hidden";
                var marketingCards = document.querySelectorAll(".marketing");
                marketingCards.forEach(marketingCard => {
                    marketingCard.style.display = "inline-block";
                });

                document.querySelector("#filter-design").checked = false;
                document.querySelector("#filter-coding").checked = false;
            } else {
                showAllCards();
            }
        }
       

        function hideAllCards() {
            var allCards = document.querySelectorAll(".card");  

            allCards.forEach(card => {
                card.style.display = "none";
            });
        }

        function showAllCards() {
            var allCards = document.querySelectorAll(".card");  

            allCards.forEach(card => {
                card.style.display = "inline-block";
            });
        }

  