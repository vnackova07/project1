<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css"
        integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
    <script src="https://kit.fontawesome.com/eefd40223d.js" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js" type="text/javascript"></script>
    <link rel="stylesheet" href="style.css">
    <title>Brainster Labs</title>
   </head>

<body>

    <nav>
        <a id="mainLogo" href="./index.html"><img src="./images/logotext.png" alt=""></a>
        <ul>
            <li><a href="https://brainster.co/marketing/" target="_blank">Академија за маркетинг</a></li>
            <li><a href="https://brainster.co/full-stack/" target="_blank">Академија за програмирање</a></li>
            <li><a href="https://brainster.co/data-science/" target="_blank">Академија за data science</a></li>
            <li><a href="https://brainster.co/graphic-design/" target="_blank">Академија за дизајн</a></li>
            <div><a href="./contact.php">Вработи наш студент</a></div>
        </ul>
        <a class="burgerMenu"><img id="burgerImage" src="./images/burgermenu.svg" alt=""></a>
    </nav>

    <div class="container-fluid yellowbg vraboti">
        <div class="container d-flex justify-content-center align-items-center">
            <h1>Вработи Студенти</h1>
        </div>
        <div class="container">
        <form method="post" action="./submit.php">
            <div class="row pt-4 pt-md-5">
                <div class="col-12 col-md-6 mt-0 mt-md-5">
                   
                    <div class="form-group">
                    <label for="exampleInputName" class="font-weight-bold">Име и презиме</label>
                    <input name="name" type="text" class="form-control form-control-lg" id="exampleInputName" required placeholder="Вашето име и презиме" style="font-style:italic; font-size: small;">
                 </div>  
                </div>
                <div class="col-12 col-md-6 mt-0 mt-md-5">
                    <div class="form-group">
                    <label for="exampleInputCompany" class="font-weight-bold">Име на компанија</label>
                    <input name="company_name" type="text" class="form-control form-control-lg" id="exampleInputCompany" required placeholder="Име на Вашата компанија" style="font-style:italic; font-size: small;">
                    </div>
                </div>
            </div>
            <div class="row pt-2 pt-md-2">
                <div class="col-12 col-md-6">
                    
                    <div class="form-group">
                    <label for="exampleInputEmail" class="font-weight-bold">Контакт имејл</label>
                    <input name="email" type="email" class="form-control form-control-lg" id="exampleInputEmail" aria-describedby="emailHelp" required placeholder="Контакт имејл на Вашата компанија" style="font-style:italic; font-size: small;">
                </div>
                    
                </div>
                <div class="col-12 col-md-6">
                    
                    <div class="form-group">
                    <label for="exampleInputPhone" class="font-weight-bold">Контакт телефон</label>
                    <input name="phone" type="number" class="form-control form-control-lg" id="exampleInputPhone" required placeholder="Контакт телефон на Вашата компанија" style="font-style:italic; font-size: small;">
                </div>
            
                </div>
            </div>

            <div class="row pt-2 pt-md-2">
                <div class="col-12 col-md-6">
                        <div class="form-group">
                        <label for="exampleInputТype" class="font-weight-bold">Тип на студенти</label>

                        <?php
                        define('DB_SERVER', 'localhost');
                        define('DB_USERNAME', 'root');
                        define('DB_PASSWORD', 'password');
                         define('DB_NAME', 'phplearning');
    
                        $link = mysqli_connect(DB_SERVER, DB_USERNAME, DB_PASSWORD, DB_NAME);
                        if($link === false){
                        die("Error: Could not connect." . mysqli_connect_error());
                        }
                        $sql = "SELECT * FROM categories";
                        if($result = mysqli_query($link, $sql)) {
                        if(mysqli_num_rows($result) > 0) {
                            echo '<select name="type" class="form-control form-control-md font-weight-bold">';
                            echo '<option value="" disabled selected hidden>Изберете тип на студент</option>';
                            while($row = mysqli_fetch_array($result)){
                            
                            echo "<option value='" . $row['id'] . "'>" . $row['category'] . "</option>";
                        
                        

                            }
                            echo "</select>";
                            mysqli_free_result($result);
                        }
                        else{
                            echo "Something went wrong...";
                        }
                        }
                        else{
                            echo "ERROR: Could not execute $sql." .mysql_error($link);
                        }
                        
                        
                            
                        ?>
                            </div>
                        </div>
                    <div class="col-12 col-md-6 pt-4">
                        <button type="submit" class="btn btn-danger redbtn btn-lg btn-block font-weight-bold form-control-md form-control">ИСПРАТИ</button>
                        </div>
    </div>
                    </form>
</div>
                        </div>
    <footer>
        <div class="container-fluid bg-dark text-white d-flex justify-content-center">
            <p class="m-0 py-3">Изработено со  &#x2764; од студентите на Brainster</p>
        </div>
    </footer>

        <script src="./main.js"></script>
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
        crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"
        integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN"
        crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.min.js"
        integrity="sha384-+YQ4JLhjyBLPDQt//I+STsc9iw4uQqACwlvpslubQzn4u2UU2UFM80nGisd026JF"
        crossorigin="anonymous"></script>
        
</body>
</html>