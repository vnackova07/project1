<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css"
        integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
    <script src="https://kit.fontawesome.com/eefd40223d.js" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js" type="text/javascript"></script>
    <link rel="stylesheet" href="style.css">
    <title>Brainster Labs</title>
   </head>

<body>
<nav>
        <a id="mainLogo" href="./index.html"><img src="./images/logotext.png" alt=""></a>
        <ul>
            <li><a href="https://brainster.co/marketing/" target="_blank">Академија за маркетинг</a></li>
            <li><a href="https://brainster.co/full-stack/" target="_blank">Академија за програмирање</a></li>
            <li><a href="https://brainster.co/data-science/" target="_blank">Академија за data science</a></li>
            <li><a href="https://brainster.co/graphic-design/" target="_blank">Академија за дизајн</a></li>
            <div><a href="./contact.php">Вработи наш студент</a></div>
        </ul>
        <a class="burgerMenu"><img id="burgerImage" src="./images/burgermenu.svg" alt=""></a>
    </nav>

    <div class="container-fluid yellowbg vraboti bg-picture">
        <div class="container d-flex justify-content-center align-items-center pt-5">
<?php

$name = $_POST['name'];
$company_name = $_POST['company_name'];
$email = $_POST['email'];
$phone = $_POST['phone'];
$type = $_POST['type'];

$servername = "localhost";
$username = "root";
$password = "password";
$dbname = "phplearning";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
  die("Connection failed: " . $conn->connect_error);
}

$sql = "INSERT INTO data (name, company_name, email, phone, type) VALUES ('$name', '$company_name', '$email', $phone, $type)";

if ($conn->query($sql) === TRUE) {
  echo "<h1>Успешна регистрација!</h1>";
} else {
  echo "Error: " . $sql . "<br>" . $conn->error;
}

$conn->close();
?>
</div>
</div>
<footer>
        <div class="container-fluid bg-dark text-white d-flex justify-content-center">
            <p class="m-0 py-3">Изработено со  &#x2764; од студентите на Brainster</p>
        </div>
    </footer>
<script src="./main.js"></script>
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
        crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"
        integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN"
        crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.min.js"
        integrity="sha384-+YQ4JLhjyBLPDQt//I+STsc9iw4uQqACwlvpslubQzn4u2UU2UFM80nGisd026JF"
        crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/1.16.1/TweenMax.min.js"></script>
</body>

</html>